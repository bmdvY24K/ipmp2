# IPMP - InterPlanetary Media Platform

IPMP 是一個旨在盡可能在 server side 實施抗審查技術的、爲新聞媒體機構設計優化的**網站/媒體內容管理系統**。  

## 只是個預覽文章的 repo

- 在Github Desktop 添加這個 repo：https://gitlab.com/bmdvy24k/ipmp2.git  
- 照常編輯/新增文章或圖片  
- push 上去會自動 build (~3 min)，會有網頁出現在這裡：https://bmdvy24k.gitlab.io/ipmp2
- 當然也可從本 repo 左側欄進入 Pipeline 查看 build 的進度（和結果狀態）。
