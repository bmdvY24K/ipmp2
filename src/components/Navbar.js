import React, { useState, useEffect } from 'react'
import { Link, withPrefix } from 'gatsby'
import useSiteMetadata from './SiteMetadata'
import '../styles/Navbar.sass'

const Navbar = () => {
  const [active, setActive] = useState(false)
  const [navBarActiveClass, setNavBarActiveClass] = useState('')
  const { title, logo } = useSiteMetadata()

  function toggleHamburger () {
    // toggle the active boolean in the state
    setActive(!active)
  }

  useEffect(() => {
    // set the class in state for the navbar accordingly
    if (active) {
      setNavBarActiveClass('is-active')
    } else {
      setNavBarActiveClass('')
    }
  }, [active])

  return (
    <nav
      className="navbar"
      role="navigation"
      aria-label="main-navigation"
    >
      <div className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item" title="Logo">
            <img src={`${withPrefix('/')}img/${logo}`} alt={title} />
          </Link>
          {/* Hamburger menu */}
          {/* eslint-disable-next-line */}
          <div
            className={`navbar-burger burger ${navBarActiveClass}`}
            data-target="navMenu"
            onClick={() => toggleHamburger()}
          >
            <span />
            <span />
            <span />
          </div>
        </div>
        <div
          id="navMenu"
          className={`navbar-menu ${navBarActiveClass}`}
        >
          <div className="navbar-start has-text-centered">
            <Link className="navbar-item" to="/trending/">
              热点
            </Link>
            <Link className="navbar-item" to="/columnist/">
              专栏
            </Link>
            <Link className="navbar-item" to="/daily-brief/">
              早报
            </Link>
          </div>
          <div className="navbar-end has-text-centered">
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
