import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'

import Layout from '../../components/Layout'
import ArticleRoll from '../../components/ArticleRoll'
import SectionTitle from '../../components/SectionTitle'

const TrendingPage = ({
  data: {
    allMarkdownRemark,
    site: {
      siteMetadata: { title },
    },
  },
}) => (
  <Layout>
    <section className="section">
      <Helmet title={`热点 | ${title}`} />
      <div className="container content">
        <div className="columns">
          <div
            className="column is-10 is-offset-1"
            style={{ marginBottom: '6rem' }}
          >
            <h2>
              <SectionTitle>热点</SectionTitle>
            </h2>
            <ArticleRoll allMarkdownRemark={allMarkdownRemark} columns="2" />
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default TrendingPage

TrendingPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
}

export const trendingPageQuery = graphql`
  query TrendingPageQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      limit: 100
      sort: { order: DESC, fields: [frontmatter___trending] }
      filter: { 
        frontmatter: { 
          templateKey: { eq: "article" },
          trending: { gt: 0 }
      } }
    ) {
      edges {
        node {
          ...articleThumbFields
        }
      }
    }
  }
`
